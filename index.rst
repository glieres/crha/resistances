.. index::
   ! CRHA (Citoyens Résistants d'Hier et d'Aujourd'hui)

.. raw:: html

   <a rel="me" href="https://kolektiva.social/@grenobleluttes"></a>
   <a rel="me" href="https://kolektiva.social/@raar"></a>
   <a rel="me" href="https://framapiaf.org/@goldman_bakounine"></a>
   <a rel="me" href="https://framapiaf.org/@pvergain"></a>
   <a rel="me" href="https://piaille.fr/@ldh_grenoble"></a>


.. figure:: images/crha_logo.png
   :align: center


|FluxWeb| `RSS <https://raar.frama.io/linkertree/rss.xml>`_

.. _glieres_crha:

====================================================================
**Glières CRHA (Citoyens Résistants d'Hier et d'Aujourd'hui)**
====================================================================


- http://www.citoyens-resistants.fr
- https://www.youtube.com/@CRHA_RassemblementdesGlieres/videos
- https://fr-fr.facebook.com/rassemblementdesglieres/
- https://nitter.net/CRHA_glieres/rss
- https://nitter.net/Gilles_Perret/rss

::

    129 rue des Fleuries , Thorens-Glières, France
    citoyen.2008@yahoo.fr

.. figure:: images/crha_5000.png
   :align: center


.. figure:: images/carte_thorens_glieres.png
   :align: center

   Thorens-Glières, cinéma Le Parnal, monument en mémoire des résistants espagnols, MJC et salle Tom Morel https://www.openstreetmap.org/#map=18/45.99726/6.24842


.. toctree::
   :maxdepth: 5

   2023/2023
   2021/2021
   2011/2011
